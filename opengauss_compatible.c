/*-------------------------------------------------------------------------
 *
 * IDENTIFICATION
 *	  opengauss_compatible.c
 *
 *-------------------------------------------------------------------------
 */
#include "opengauss_compatible.h"
#include "utils/builtins.h"

XLogRecPtr
text_to_lsn(text *location)
{
	uint32		hi = 0;
    uint32 		lo = 0;
	XLogRecPtr	lsn = 0;
	char	*lsn_str = text_to_cstring(location);

	if (sscanf_s(lsn_str, "%X/%X", &hi, &lo) != 2)
        elog(ERROR,"could not parse transaction log location \"%s\"", lsn_str);

    lsn = (((uint64)hi) << 32) | lo;
	return lsn;
}

text*
lsn_to_text(XLogRecPtr lsn)
{
	char location[MAXFNAMELEN];
	int errorno = 0;

	errorno = snprintf_s(location,
        sizeof(location),
        sizeof(location) - 1,
        "%X/%X",
        (uint32)(lsn >> 32),
        (uint32)lsn);
    if(-1 == errorno)
	{
		elog(ERROR, "Could not turn it");
	}
    return cstring_to_text(location);
}
