/*-------------------------------------------------------------------------
 *
 * IDENTIFICATION
 *	  wal2sql_ddl.h
 *
 *-------------------------------------------------------------------------
 */
#ifndef WALMINER_DDL_H
#define WALMINER_DDL_H

#include "walminer_decode.h"
#include "replication/reorderbuffer.h"

typedef enum DDLKind
{
    DDLNO_TABLE_CREATE = 0,
    DDLNO_TABLE_DROP,
    DDLNO_TABLE_TRUNCATE,

    DDLNO_INDEX_CREATE,
    DDLNO_INDEX_DROP,

    DDLNO_MAX
}DDLKind;

typedef struct DDLData
{
    DDLKind     ddlKind;
    bool        inddl;
    /* 表相关 */
    NameData    nspName;
    NameData    relName;
    Oid         reloid;
    char        relpersistence;
    List        *attList;

}DDLData;

typedef struct AttItem
{
    NameData    attName;
    NameData    addKindName;
    Oid         attKindOid;
    int         attnum;
}AttItem;

void init_ddl_analyse(void);
void ddl_handle(ReorderBufferChange *change, TransactionEntry *te);

#endif