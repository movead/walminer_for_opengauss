/*-------------------------------------------------------------------------
 *
 * IDENTIFICATION
 *	  opengauss_compatible.h
 *
 *-------------------------------------------------------------------------
 */
#ifndef TEMP_H
#define TEMP_H

#include "postgres.h"
#include "storage/sinval.h"
#include "access/twophase.h"
#include "knl/knl_thread.h"
#include "access/xact.h"

typedef struct xl_xact_parsed_commit
{
	TimestampTz xact_time;
	uint32		xinfo;

	Oid			dbId;			/* MyDatabaseId */
	Oid			tsId;			/* MyDatabaseTableSpace */

	int			nsubxacts;
	TransactionId *subxacts;

	int			nrels;
	RelFileNode *xnodes;

	int			nmsgs;
	SharedInvalidationMessage *msgs;

	TransactionId twophase_xid; /* only for 2PC */
	char		twophase_gid[GIDSIZE];	/* only for 2PC */
	int			nabortrels;		/* only for 2PC */
	RelFileNode *abortnodes;	/* only for 2PC */

	XLogRecPtr	origin_lsn;
	TimestampTz origin_timestamp;
} xl_xact_parsed_commit;


typedef struct xl_xact_parsed_abort
{
	TimestampTz xact_time;
	uint32		xinfo;

	Oid			dbId;			/* MyDatabaseId */
	Oid			tsId;			/* MyDatabaseTableSpace */

	int			nsubxacts;
	TransactionId *subxacts;

	int			nrels;
	RelFileNode *xnodes;

	TransactionId twophase_xid; /* only for 2PC */
	char		twophase_gid[GIDSIZE];	/* only for 2PC */

	XLogRecPtr	origin_lsn;
	TimestampTz origin_timestamp;
} xl_xact_parsed_abort;

#define HeapTupleHeaderSetXmin_OG(tup, xid) \
( \
	(tup)->t_choice.t_heap.t_xmin = (xid) \
)

#define HeapTupleHeaderSetXmax_OG(tup, xid) \
( \
	(tup)->t_choice.t_heap.t_xmax = (xid) \
)

XLogRecPtr text_to_lsn(text *location);
text* lsn_to_text(XLogRecPtr lsn);

#endif